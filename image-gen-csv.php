<?php


class ImageGenController {
	
	function __construct() { 
		
	}
	
	public function init() {
		$this->output("intro","\n*****************************\n****** Image Generator ******\n****** v 0.1 - T.Daniel *****\n*****************************");
		$file = $this->makeMenu("\nPlease Choose a File to Process:", $this->getReadableFiles());
	}
	
	public function getReadableFiles() { // Find the XML files

		$file_arr = [];
		foreach(glob('csv-input/*.csv') as $filename){
			array_push($file_arr, $filename);
		};
		
		return( $file_arr );
	}
	
	public function makeMenu($message, $array) { //Create a menu
		fwrite(STDOUT, $message); 
		$choices = $array;
		$truefalse = array("yes","no");
		fwrite(STDOUT, "(q to quit)\n"); 
		
		foreach ( $choices as $choice => $pick ) { // Loop until they enter 'q' for Quit
			
			$basename = basename($pick, ".xml");
			fwrite(STDOUT, "\t$choice: $pick\n");
			
		} do { 
			do { $selection =  chop(fgets(STDIN)); } while ( trim($selection) == '' ); 
			if ( array_key_exists($selection,$choices) ) 
			{ 
				$file = $choices[$selection];
				
				fwrite(STDOUT, "Run in test mode?\n");
				foreach ( $truefalse as $choice => $pick ) { // Loop until they enter 'q' for Quit
					fwrite(STDOUT, "\t$choice: $pick\n");
				}
				do { $mode =  chop(fgets(STDIN)); } while ( trim($selection) == '' );
				if ( array_key_exists($mode,$truefalse) ) {
					/******** Call the request *********/
					$this->process_request($file, $truefalse[$mode]);
				}
			}
		} while ( $selection != 'q' ); 
		exit(0);
	}
	
	private function process_request($file, $testmode) {
			
			if ($testmode == "no"){
			$testmode = false;
			}else{
			$testmode = true;
			};
			
			$theParser = new CSVparser($file, $testmode, $this);
			//$this->output("message","file: ".$file." | test mode: ". $testmode);
			die();
		
	}
	
	public function output($type = "notice",$message = ""){
		/* Colors, only for linux ::sad tuba::
		$colors = new Colors();
		if($type=="error"){
			fwrite(STDOUT, $colors->getColoredString("[".$type."] ".$message, "black", "red") . "\n");
		}elseif($type=="success"){
			fwrite(STDOUT, $colors->getColoredString("[".$type."] ".$message, "green", "black") . "\n");
		}elseif($type=="intro"){
			fwrite(STDOUT, $colors->getColoredString($message, "yellow", "black") . "\n");
		}else{
		*/
			fwrite(STDOUT, "[".$type."] ".$message."\n");
		//}
	}
	
};

class CSVparser {

	private $file;
	private $testmode;
	private $controller;
	
    function __construct($file = 'test.xml', $testmode = true, $parent) {
		
		$this->controller = $parent;
		
		/* Start Timer */
		$mtime = microtime(); 
		$mtime = explode(" ",$mtime); 
		$mtime = $mtime[1] + $mtime[0]; 
		$starttime = $mtime; 
		/* timer */
	
		$this->file = $file;
		$this->testmode = $testmode;
		
		 $this->controller->output("notice","****** Starting Image Generation ******");
		 $this->controller->output("success","\nfile: ".$file."\nmode: ". $testmode);
		
		$this->parseCSV($this->file);
		
		/* End timer */
		$mtime = microtime(); 
	    $mtime = explode(" ",$mtime); 
	    $mtime = $mtime[1] + $mtime[0]; 
	    $endtime = $mtime; 
	    $totaltime = ($endtime - $starttime); 
		/* timer */
		
		 $this->controller->output("success",'Run at: '.date('Y-m-d H:i:s').'. Took '.$totaltime.'s.');
	}
	
	// Returns the contents of a file
	private function file_contents($path) {
		$str = @file_get_contents($path);
		if ($str === FALSE) {
			throw new Exception("Cannot access '$path' to read contents.\n");
		} else {
			return $str;
		}
	}

	private function parseCSV($file) {
		
		// get the file
		try {
			$xml = $this->file_contents($this->file);
		} catch (Exception $e) {
			 $this->controller->output("error",$e->getMessage());
			die;
		};
		
		//$handle = fopen($file, "r");
		//$data = fgetcsv($handle);
		$data = file($file);
		
		//print_r($data);
		
		 $this->controller->output("success", $this->file." parsed. ".count($data)." entries found.");
		
		
		if($this->testmode==true){
			/*
			$this->controller->output("testtarget", $this->testtarget);
			if( array_key_exists($this->testtarget, $data) ){
				 $this->controller->output("preview", htmlspecialchars(print_r($data[$this->testtarget],true)));
			} else {
				 $this->controller->output("preview", "Sorry, that index doesn't exist. Try Again.");
			};
			*/
			$tmp = $data[0];
			$data = array();
			$data[0] = $tmp;
		};
		
		$this->generator($data);
	}
	
	private function generator($data) {
			$this->makeImages($data);
	}
	
	private function makeImages($data){
		
		$dataLength = count($data);
		$modified = 0;
		$skipped = 0;
		$skippedTitles = "";
		$count = 1;
		
		
		foreach ($data as $page) {
			
			$fmpID = (string) $page;// FMP-ID	
			
			if($fmpID != ""){
			
				//$cleanName = substr_replace(trim($fmpID), ',', -3, 0);
				$cleanName = $fmpID;
				$fileName = str_replace(' ', '-', trim($cleanName));
				$fileName = str_replace(',', '', trim($fileName));
				
				/******* iMagick heavy lifting *******/
				
				
				$canvas_image ='templates/bg-template.png';
				$dest_filename = 'output/'.$fileName.'-background-screening.png';
				$text = $cleanName.' Background Screening';
				
								/*

				$canvas_image ='templates/dna-template.png';
				$dest_filename = 'output/'.$fileName.'-DNA-test.png';
				$text = $cleanName.' DNA Testing';
				*/
				/*
				$canvas_image ='templates/drug-template.png';
				$dest_filename = 'output/'.$fileName.'-drug-test.png';
				$text = $cleanName.' Drug Testing';
				*/
				
				$font_file = 'Open-Sans';
				
				$this->autofit_text_to_image($canvas_image, $dest_filename, $text, 18, 300, 30, 45, 143, $font_file, "white");
				
				
				$this->controller->output("message","(".$count."/".$dataLength.") Created image for ".$cleanName);
				$modified++;
			}else{ // no match found, skip it
				$skipped++;
				$skippedTitles.= $fmpID."\n";
			};
			
			$count++;

		};
		$this->controller->output("message",$modified." image(s) created. ");
		if($skipped>0){
		$this->controller->output("error", "Skipped Locations:\n".$skippedTitles);
		};
		
	}
	
	private function autofit_text_to_image( $canvas_image_filename = false, $dest_filename = 'output.jpg', $text = '', $starting_font_size = 60, $max_width = 500, $max_height = 500, $x_pos = 0, $y_pos = 0, $font_file = false, $font_color = 'black', $line_height_ratio = 1, $dest_format = 'jpg' ) {
		// Bail if any essential parameters are missing
		if ( ! $canvas_image_filename || ! $dest_filename || empty($text) || ! $font_file || empty($font_color) || empty($max_width) || empty($max_height) ) return false;
		
		// Do we have a valid canvas image?
		if ( ! file_exists($canvas_image_filename) ) return;
		
		$canvas_handle = fopen( $canvas_image_filename, 'rb' );
		
		// Load image into Imagick
		$NewImage = new Imagick();
		$NewImage->readImageFile($canvas_handle);
		
		// Instantiate Imagick utility objects
		$draw = new ImagickDraw();
		$pixel = new ImagickPixel( $font_color );
		
		// Load Font 
		$font_size = $starting_font_size;
		$draw->setFont($font_file);
		$draw->setFontSize($font_size);
		$draw->setFillColor( $font_color );
		
		// Holds calculated height of lines with given font, font size
		$total_height = 0;
		
		// Run until we find a font size that doesn't exceed $max_height in pixels
		while ( 0 == $total_height || $total_height > $max_height ) {
			if ( $total_height > 0 ) $font_size--; // we're still over height, decrement font size and try again
			
			$draw->setFontSize($font_size);
			
			// Calculate number of lines / line height
			// Props users Sarke / BMiner: http://stackoverflow.com/questions/5746537/how-can-i-wrap-text-using-imagick-in-php-so-that-it-is-drawn-as-multiline-text
			$words = preg_split('%\s%', $text, -1, PREG_SPLIT_NO_EMPTY);
			$lines = array();
			$i = 0;
			$line_height = 0;
		
			while ( count($words) > 0 ) { 
				$metrics = $NewImage->queryFontMetrics( $draw, implode(' ', array_slice($words, 0, ++$i) ) );
				$line_height = max( $metrics['textHeight'], $line_height );
		
				if ( $metrics['textWidth'] > $max_width || count($words) < $i ) {
					$lines[] = implode( ' ', array_slice($words, 0, --$i) );
					$words = array_slice( $words, $i );
					$i = 0;
				}
			}
			
			$total_height = count($lines) * $line_height * $line_height_ratio;
			
			if ( $total_height === 0 ) return false; // don't run endlessly if something goes wrong
		}
		
		// Writes text to image
		for( $i = 0; $i < count($lines); $i++ ) {
			$NewImage->annotateImage( $draw, $x_pos, $y_pos + ($i * $line_height * $line_height_ratio), 0, $lines[$i] );	
		}
		
		$NewImage->setImageFormat($dest_format);
		$result = $NewImage->writeImage($dest_filename);
		
		return $result;
	}
	

}

/******* UTILS **********/

class Colors {
 private $foreground_colors = array();
 private $background_colors = array();
 
 public function __construct() {
 // Set up shell colors
 $this->foreground_colors['black'] = '0;30';
 $this->foreground_colors['dark_gray'] = '1;30';
 $this->foreground_colors['blue'] = '0;34';
 $this->foreground_colors['light_blue'] = '1;34';
 $this->foreground_colors['green'] = '0;32';
 $this->foreground_colors['light_green'] = '1;32';
 $this->foreground_colors['cyan'] = '0;36';
 $this->foreground_colors['light_cyan'] = '1;36';
 $this->foreground_colors['red'] = '0;31';
 $this->foreground_colors['light_red'] = '1;31';
 $this->foreground_colors['purple'] = '0;35';
 $this->foreground_colors['light_purple'] = '1;35';
 $this->foreground_colors['brown'] = '0;33';
 $this->foreground_colors['yellow'] = '1;33';
 $this->foreground_colors['light_gray'] = '0;37';
 $this->foreground_colors['white'] = '1;37';
 
 $this->background_colors['black'] = '40';
 $this->background_colors['red'] = '41';
 $this->background_colors['green'] = '42';
 $this->background_colors['yellow'] = '43';
 $this->background_colors['blue'] = '44';
 $this->background_colors['magenta'] = '45';
 $this->background_colors['cyan'] = '46';
 $this->background_colors['light_gray'] = '47';
 }
 
 // Returns colored string
 public function getColoredString($string, $foreground_color = null, $background_color = null) {
 $colored_string = "";
 
 // Check if given foreground color found
 if (isset($this->foreground_colors[$foreground_color])) {
 $colored_string .= "\033[" . $this->foreground_colors[$foreground_color] . "m";
 }
 // Check if given background color found
 if (isset($this->background_colors[$background_color])) {
 $colored_string .= "\033[" . $this->background_colors[$background_color] . "m";
 }
 
 // Add string and end coloring
 $colored_string .=  $string . "\033[0m";
 
 return $colored_string;
 }
 
 // Returns all foreground color names
 public function getForegroundColors() {
 return array_keys($this->foreground_colors);
 }
 
 // Returns all background color names
 public function getBackgroundColors() {
 return array_keys($this->background_colors);
 }
 }
 
 
 
 
 /********** INIT *************/
 $controller = new ImageGenController();
 $controller->init();
 
?>